const showClock = () => {
    let currentlyDate = new Date();
    let h = hourFormat(currentlyDate.getHours());
    let m = hourFormat(currentlyDate.getMinutes());
    let s = hourFormat(currentlyDate.getSeconds());
    document.getElementById('hour').innerHTML = `${h}:${m}:${s}`;

    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep","Oct","Nov","Dec"];
    const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    let day = days[currentlyDate.getDay()];
    let month = months[currentlyDate.getMonth()];
    let textDate = `${day}, ${currentlyDate.getDate()} ${month} ${currentlyDate.getFullYear()}`;
    document.getElementById('date').innerHTML = textDate;

}

const hourFormat = (hour) => {
    if(hour < 10)
        hour = "0" + hour;
    return hour;
}

setInterval(showClock, 1000);